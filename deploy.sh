rm -rf istio-1.*
curl -L https://istio.io/downloadIstio | ISTIO_VERSION=1.6.5 sh -
cd istio-1.6.5
export PATH=$PWD/bin:$PATH
istioctl install --set profile=demo

cd ../springio-api
mvn clean package -DskipTests=true 
docker build -t springio-api .
docker tag springio-api re102162189/springio-api:1.0.0
docker push re102162189/springio-api:1.0.0 


cd ../springio-db
mvn clean package -DskipTests=true 
docker build -t springio-db .
docker tag springio-db re102162189/springio-db:1.0.0
docker push re102162189/springio-db:1.0.0

kubectl delete namespace springio
kubectl delete pv mysql-pv --grace-period=0 --force

cd ..
kubectl create namespace springio

kubectl apply -f k8s/base
kubectl apply -f k8s/istio
kubectl apply -f k8s/mysql  
kubectl apply -f springio-api/k8s-app 
kubectl apply -f springio-db/k8s-app  

kubectl label namespace springio istio-injection=enabled

kubectl -n springio rollout restart deployment springio-api springio-db mysql

kubectl get pods -n springio