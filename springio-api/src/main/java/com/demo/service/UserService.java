package com.demo.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.client.RestTemplate;
import com.demo.model.User;

@Service
public class UserService {
  private static final Logger logger = LoggerFactory.getLogger(UserService.class);

  private static final String USER_URL = "http://springio-db:8080/db/users/";

  public User getUser(Long id) {
    logger.info("send user id to db rest: {}", id);

    RestTemplate restTemplate = new RestTemplate();
    User user = restTemplate.getForObject(USER_URL + id, User.class);
    
    logger.info("get user from db rest: {}", user);
    return user;
  }

  public void save(User user) {
    //
  }

  public void update(User user) {
    //
  }

  public void delete(@PathVariable("id") Long id) {
    //
  }
}
