### Prerequirements
* docker (v19.03.13) [install](https://docs.docker.com/engine/install/)
* enable kubernetes (v1.19.3) in docker preference
* istio (1.6.5) [install](https://istio.io/docs/setup/getting-started/)
* Test under macOS High Sierra 10.13.4, CPU: 2.5GHz Interl Core i7 / RAM: 16GB 2133MHz LPDDR3

### pack spring boot & build image, then push image to docker hub  
```
$ cd springio-api
$ mvn clean package -DskipTests=true
$ docker build -t springio-api .

$ cd springio-db
$ mvn clean package -DskipTests=true
$ docker build -t springio-db .

# this image tag and "image" (in k8s-app/deployment.yaml) should be identical
#
$ docker login
$ docker tag springio-api ${your_docker_hub}/springio-api:1.0.0
$ docker tag springio-db ${your_docker_hub}/springio-db:1.0.0
$ docker push ${your_docker_hub}/springio-api:1.0.0
$ docker push ${your_docker_hub}/springio-db:1.0.0 
```

======================= Service Mesh istio =======================
### install istio
```
$ curl -L https://istio.io/downloadIstio | ISTIO_VERSION=1.6.5 sh -
$ cd istio-1.6.5
$ export PATH=$PWD/bin:$PATH
$ istioctl install --set profile=demo
```

### check k8s cluster status  
```
$ kubectl cluster-info
$ kubectl get nodes
$ kubectl get services
```

### create a k8s namespace for demo  
```
$ kubectl create namespace springio
$ kubectl get namespace
```

### apply all service (app & mysql & istio)  
```
$ kubectl apply -f k8s/base
$ kubectl apply -f k8s/istio
$ kubectl apply -f k8s/mysql

$ kubectl apply -f springio-api/k8s-app
$ kubectl apply -f springio-db/k8s-app
```

### check service,pod status  
```
$ kubectl get svc,pods -n springio
```

### inject service mesh to namespace springio
```
$ kubectl label namespace springio istio-injection=enabled

# restart pods to adopt injection
#
$ kubectl -n springio rollout restart deployment springio-api springio-db mysql

# verify injection
#
$ istioctl analyze -n springio
```

### check swagger-ui, add "springio.info" as hostname in /etc/hosts (e.g. Mac OSX)  
```
$ sudo vi /etc/hosts  

# ip from ignress status
# check swagger-ui: http://springio.info/swagger-ui.html
#
127.0.0.1 springio.info  
```

### or using kubernetes proxy
```
# open browser: http://localhost:8080/swagger-ui.html
#
$ kubectl port-forward -n springio svc/springio-api 8080:8080
```

========================== dashboard ===============================
### check prometheus  
```
# open browser: http://localhost:9090/
# 
$ kubectl port-forward -n istio-system svc/prometheus 9090:9090
```

### check grafana  
```
# open browser: http://localhost:3000/
# import springboot dashboard: k8s/grafana/springboot.json
# 
$ kubectl port-forward -n istio-system svc/grafana 3000:3000
```

### check kiali   
```
# default username:pwd = admin:admin
#
$ istioctl dashboard kiali
```

### check kubernates [dashboard](https://kubernetes.io/docs/tasks/access-application-cluster/web-ui-dashboard/)  
```
# open browser: http://localhost:8001/api/v1/namespaces/kubernetes-dashboard/services/https:kubernetes-dashboard:/proxy/
# create role [sample user](https://github.com/kubernetes/dashboard/blob/master/docs/user/access-control/creating-sample-user.md)
#
$ kubectl proxy
$ kubectl -n kubernetes-dashboard describe secret $(kubectl -n kubernetes-dashboard get secret | grep admin-user | awk '{print $1}')
```

========================== reset ===============================
### delete all in namespace "springio" & PV  
```
$ kubectl delete namespaces springio
$ kubectl delete pv mysql-pv --grace-period=0 --force
```

========================== debug ===============================
### debug pod & check pod logs  
```
$ kubectl describe pods ${POD_NAME} -n springio 
$ kubectl logs ${POD_NAME} -n springio   
```

### look inside mysql  
```
$ kubectl -n springio exec -it ${MYSQL_POD_NAME} -- mysql -u root -p
```

### if pods stuck in terminating status  
```
$ kubectl delete pod <PODNAME> --grace-period=0 --force -n springio  
```

### check docker tmp files  
```
$ screen ~/Library/Containers/com.docker.docker/Data/vms/0/tty  
$ cd /var/lib/docker/containers  
```

========================== kubernetes ingress (other proxy approach) =======================
### enable ingress & check ingress status  
```
$ kubectl apply -f k8s/ingress
$ kubectl get ingress -n springio  

NAME      HOSTS           ADDRESS          PORTS   AGE  
ingress   springio.info   192.168.99.105   80      21m  
```

### add "springio.info" as hostname in /etc/hosts (e.g. Mac OSX)  
```
$ sudo vi /etc/hosts  

# ip from ignress status
# check swagger-ui: http://springio.info/swagger-ui.html
#
192.168.99.105 springio.info  
```